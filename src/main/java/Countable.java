/**
 * Created by Filip on 2017-07-20.
 */
public interface Countable {

    int counter(String pathToFile);
}
